﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeDisplayScript : MonoBehaviour {

	public GameObject timeDisplayPanel;
	public Text timeDisplayTxt;

	private GameHandler gameHandler;

	// Use this for initialization
	void Start () {
		gameHandler = FindObjectOfType<GameHandler>();
	}
	
	// Update is called once per frame
	void Update () {
		if(gameHandler != null) {
			timeDisplayPanel.SetActive(gameHandler.isGameStart);
			timeDisplayTxt.text = gameHandler.currentTimeCount.ToString("00");
		}
		else {
			timeDisplayPanel.SetActive(false);
			gameHandler = FindObjectOfType<GameHandler>();
		}
	}
}
