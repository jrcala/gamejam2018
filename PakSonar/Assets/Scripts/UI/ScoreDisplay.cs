﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour {

	public Transform scorePanel;
	public GameObject scoreText;

	private NetworkPlayerHandler[] networkPlayers;
	private List<GameObject> scoreHolders = new List<GameObject>();
	
	// Update is called once per frame
	void Update () {
		networkPlayers = FindObjectsOfType<NetworkPlayerHandler>();
		RefreshScoreList();
	}

	private void RefreshScoreList() {
		for(int i = 0; i < scoreHolders.Count; i++) {
			Destroy(scoreHolders[i]);
		}
		scoreHolders.Clear();

		for(int i = 0; i < networkPlayers.Length; i++) {
			GameObject scoreObj = GameObject.Instantiate<GameObject>(scoreText, scorePanel, false);
			scoreObj.GetComponentInChildren<Text>().text = networkPlayers[i].score.ToString();
			scoreHolders.Add(scoreObj);
		}
	}
}
