﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IPDisplay : MonoBehaviour {

	public Text ipTxt;

	// Use this for initialization
	void Start () {
		string ip = PlayerPrefs.GetString("ServerIP");

		ipTxt.text = "IP: " + ip;	
	}
	
}
