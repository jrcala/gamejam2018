﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class EndGamePanelHandler : NetworkBehaviour {

	public GameObject EndGamePanel;

	public Text GameEndText;

	public Button RematchButton;
	public Button ExitButton;

	public void OnEnable() {
		
	}

	public void ShowPanel() {
		int place = 0;
		EndGamePanel.SetActive(true);
		NetworkPlayerHandler player = NetworkPlayerHandler.MyOwn;
		NetworkPlayerHandler[] players = FindObjectsOfType<NetworkPlayerHandler>();

		for (int i = 0; i < players.Length; i++) {
			if (player != players[i]) {
				if (player.score >= players[i].score) {
					place++;
				}
			}
		}

		int resultPlace = players.Length - place;
		string suffix = "st";

		if (resultPlace == 2) {
			suffix = "nd";
		}
		else if (resultPlace == 3) {
			suffix = "rd";
		}
		else {
			suffix = "th";
		}

		GameEndText.text = "Game Ended!\n YOU GOT " + player.score + " POINTS!";

		if (isServer) {
			RematchButton.gameObject.SetActive(true);
		}

		
		RematchButton.gameObject.SetActive(isServer);
		ExitButton.gameObject.SetActive(isServer);


		
	}

	public void HidePanel() {
		EndGamePanel.SetActive(false);
	}

	// Update is called once per frame
	void Update () {
		
	}
}
