﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerupDisplay : MonoBehaviour {
	public Sprite[] sprites;

	public Image sprite;
	
	void Start(){
		sprite.enabled = false;
	}

	public void SetPowerup(PowerupPickup.Powerups powerup){
		if(PowerupPickup.Powerups.NONE == powerup){
			sprite.enabled = false;
			return;
		}
		else{
			sprite.enabled = true;
		}

		sprite.sprite = sprites[(int)powerup];

	}
}
