﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CameraFollow : MonoBehaviour
{

    public GameObject player;
    private Vector3 offset;
	
    // Use this for initialization
    void Start()
    {
        if(player != null) {
			Reconfig();
		}
    }

	public void Reconfig() {
		Vector3 newpos = transform.position;
		newpos.x = player.transform.position.x;
		newpos.y = player.transform.position.y;

		transform.position = newpos;

		offset = transform.position - player.transform.position;
	}

	void LateUpdate() {
		if (player != null) {
			if (player.transform != null)
				transform.position = player.transform.position + offset;
		}
	}
}