﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitingForPlayersController : MonoBehaviour {

	public GameObject WaitingForPlayersPanel;
	private GameHandler gameHandler;

	// Use this for initialization
	void Start () {
		gameHandler = FindObjectOfType<GameHandler>();
	}

	// Update is called once per frame
	void Update() {
		if (gameHandler == null) gameHandler = FindObjectOfType<GameHandler>();
		else {
			WaitingForPlayersPanel.SetActive(!gameHandler.isGameStart && !gameHandler.isEndGame);
		}
	}
}
