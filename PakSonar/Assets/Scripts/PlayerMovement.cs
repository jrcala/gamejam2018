﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public enum Direction
{
    UP,
    DOWN,
    LEFT,
    RIGHT
}

public class PlayerMovement : NetworkBehaviour
{
    public GameObject spriteHolder;
    //Controls
    [SerializeField] private KeyCode moveUp = KeyCode.W;
    [SerializeField] private KeyCode moveDown = KeyCode.S;
    [SerializeField] private KeyCode moveRight = KeyCode.D;
    [SerializeField] private KeyCode moveLeft = KeyCode.A;
    //POWER UP
    [SerializeField] private float currentSpeed = 1;
    [SerializeField] private const float normalSpeed = 10;
    [SerializeField] private float powerSpeedDuration = 5;
    [SerializeField] private float speedIncrement = 5f;
    [SerializeField] private float speedLimit;
    [SerializeField] private float powerShiftDuration = 1;
	[SerializeField] private float shieldDuration = 5.0f;
    [SerializeField] private int ShiftLayer = 10;
    public bool isPoweredUp;

	public GameHandler gameHandler;
    //Others
    private SpriteRenderer _spriteRenderer;
    private Vector2 _currentVelocity;
	public PowerupPickup.Powerups equippedPowerup = PowerupPickup.Powerups.NONE;
    ConstantForce2D cf2d;
    Rigidbody2D rgb2;
    Collider2D coll2;
    public Direction facing = Direction.RIGHT;

    private CameraFollow cameraFollow;
	private PowerupDisplay powerUpDislay;

	private Animator anim;

    // Use this for initialization
    void Start()
    { 
		gameHandler = GameObject.Find("GameHandler").GetComponent<GameHandler>();
        isPoweredUp = false;
	
        speedLimit = normalSpeed + speedIncrement;
        cf2d = GetComponent<ConstantForce2D>();
        rgb2 = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
		if(spriteHolder != null)
		_spriteRenderer = spriteHolder.GetComponent<SpriteRenderer>();
		if(this.isLocalPlayer){
			this.powerUpDislay = GameObject.Find("PowerupPanel").GetComponent<PowerupDisplay>();
		}
    }

    void FixedUpdate()
    {
        //if (!isLocalPlayer) return;
		if(!gameHandler.isGameStart) return;
        //Debug.Log("current:" + rgb2.velocity);
//        rgb2.velocity = _currentVelocity;
        if (Input.GetKey(moveUp))
        {
            rgb2.velocity = Vector2.up * currentSpeed;
            facing = Direction.UP;
        }
        else if (Input.GetKey(moveDown))
        {
            rgb2.velocity = Vector2.down * currentSpeed;
            facing = Direction.DOWN;
        }
        else if (Input.GetKey(moveRight))
        {
            rgb2.velocity = Vector2.right * currentSpeed;
            facing = Direction.RIGHT;
            if (_spriteRenderer != null)
                _spriteRenderer.flipX = true;
        }
        else if (Input.GetKey(moveLeft))
        {
            rgb2.velocity = Vector2.left * currentSpeed;
            facing = Direction.LEFT;
            if (_spriteRenderer != null)
                _spriteRenderer.flipX = false;
        }

		if (Input.GetKeyDown(KeyCode.I)) {
			TriggerPower();
		}

		anim.SetFloat("xSpeed", -rgb2.velocity.x);
		anim.SetFloat("ySpeed", rgb2.velocity.y);
    }

    void OnCollisionEnter2D(Collision2D hit)
    {
        if (!isLocalPlayer) return;
      	 _currentVelocity = Vector2.zero;
    }

	public void PickupPowerup(PowerupPickup.Powerups powerup){
		this.equippedPowerup = powerup;
		if(powerUpDislay != null) this.powerUpDislay.SetPowerup(powerup);
	}

	public void TriggerPower(){
		switch(equippedPowerup){
			case PowerupPickup.Powerups.BEACON:	
			ActivatePower("ActivateBeacon");
			break;
			case PowerupPickup.Powerups.SHIELD:	
			ActivatePower("ActivateShield");
			break;
			case PowerupPickup.Powerups.SPEED:	
			ActivatePower("ActivateSpeed");
			break;
		}


		this.equippedPowerup = PowerupPickup.Powerups.NONE;
		if(powerUpDislay != null) this.powerUpDislay.SetPowerup(equippedPowerup);
	}

    public void ActivatePower(string powerUp)
    {
        if (isPoweredUp)
        {
            Debug.Log("cancelled");
            return;
        }
        else
        {
            Debug.Log("Power!");
            StartCoroutine(powerUp);
            isPoweredUp = true;
        }
    }

	public IEnumerator ActivateBeacon(){
		Debug.Log("Beacon activated");
		this.GetComponent<NetworkPlayerHandler>().CmdBeaconSpawn(gameObject);
		isPoweredUp = false;
		yield return null;
	}


    public IEnumerator ActivateShift()
    {
        Debug.Log("Shift activated");
        //walls and other game object needed to be transfered to layer: Obstacles
       
        //Change the player's layer to "Shifted" 
        int tempLayer = gameObject.layer;
        this.gameObject.layer = ShiftLayer;
        yield return new WaitForSeconds(powerSpeedDuration);
        this.gameObject.layer = tempLayer;
		isPoweredUp = false;
        Debug.Log("Shift deactivated");
    }

	public IEnumerator ActivateShield(){
		Debug.Log("Shield activated");
		NetworkPlayerHandler networkPlayer = this.GetComponent<NetworkPlayerHandler>();
		networkPlayer.isShield = true;
		yield return new WaitForSeconds(shieldDuration);
		networkPlayer.isShield = false;
		isPoweredUp = false;
	}

    public IEnumerator ActivateSpeed()
    {
        Debug.Log("speed activated");
        float tempSpeed = currentSpeed;
        currentSpeed = speedLimit;
        yield return new WaitForSeconds(powerShiftDuration);
        currentSpeed = tempSpeed; 
        Debug.Log("speed deactivated");
		isPoweredUp = false;
    }
}
