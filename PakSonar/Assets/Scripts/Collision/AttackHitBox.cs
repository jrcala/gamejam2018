﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHitBox : MonoBehaviour {

	public GameObject parent;

	public List<GameObject> targets = new List<GameObject>();

	public Direction facing = Direction.RIGHT;
	
	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag != "Player" || coll.gameObject.GetInstanceID() == parent.GetInstanceID()) return;
		targets.Add(coll.gameObject);
	}

	void OnTriggerExit2D(Collider2D coll){
		if(coll.gameObject.tag != "Player" || coll.gameObject.GetInstanceID() == parent.GetInstanceID()) return;
		targets.Remove(coll.gameObject);
	}
}
