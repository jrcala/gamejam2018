﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Net;

public class SetupGameController : MonoBehaviour {

	public Text ipText;

	private NetworkManager networkManager;
	private string serverIP = "";

	private void OnEnable() {
		networkManager = FindObjectOfType<NetworkManager>();
	}

	// Use this for initialization
	void Start () {
		
	}

	public void StartServer() {
		networkManager.StartHost();
		IPAddress[] ips = Dns.GetHostAddresses(Dns.GetHostName());
		
		foreach (IPAddress ip in ips) {
			if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
				serverIP = ip.ToString();
				break;
			}
		}

		if(!string.IsNullOrEmpty(serverIP))
		PlayerPrefs.SetString("ServerIP", serverIP);
	}

	public void JoinServer() {
		networkManager.networkAddress = ipText.text;
		networkManager.StartClient();
		PlayerPrefs.SetString("ServerIP", "");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
