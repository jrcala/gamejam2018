﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameHandler : NetworkBehaviour {

	public int currentPlayers = 0;

	public int maxPlayer = 2;

	public float endTimeDuration = 10;

	public bool isEndGame = false;

	[SyncVar]
	public bool isForceStart = false;

	public Transform[] spawnPoints;

	[SyncVar]
	public float currentTimeCount;

	public bool isGameStart{

		get{
			return currentPlayers >= maxPlayer && !isEndGame || isForceStart;
		}
	}

	public void AddPlayer(){
		currentPlayers++;
		if(currentPlayers >= maxPlayer){
			if(this.isServer){
				//this.Invoke("InvokeEndGame", endTimeDuration);
				StartGame();
				RpcStartGame();
			}
		}
	}

	public void ForceStartGame(){
		isForceStart = true;
		StartGame();
		RpcStartGame();
	}

	public void Respawn(NetworkPlayerHandler player){
		Transform target = this.spawnPoints[Random.Range(0,this.spawnPoints.Length - 1 )];
		player.transform.position = target.position;
	}

	[ClientRpc]
	void RpcStartGame(){

	}

	public void ResetGame() {
		if (isServer) {
			NetworkPlayerHandler[] networkPlayers = FindObjectsOfType<NetworkPlayerHandler>();

			foreach(NetworkPlayerHandler nwPlayer in networkPlayers) {
				Respawn(nwPlayer);
				nwPlayer.score = 0;
			}

			isEndGame = false;
			currentTimeCount = endTimeDuration;

			FindObjectOfType<EndGamePanelHandler>().HidePanel();
		}
	}

	void StartGame(){
		Debug.Log("StartedGame!");
		if (isServer) {
			currentTimeCount = endTimeDuration;
		}
	}

	public void InvokeEndGame(){
		EndGame();
		RpcEndGame();
	}

	public void Update() {
		if (isServer) {
			if (isGameStart) {
				currentTimeCount -= Time.deltaTime;
				currentTimeCount = Mathf.Max(currentTimeCount, 0);

				if (currentTimeCount <= 0) {
					EndGame();
					RpcEndGame();
				}
			}
		}
		else {
			if(currentTimeCount <= 0 && isGameStart) {
				if (!this.isEndGame) {
					this.isEndGame = true;
					FindObjectOfType<EndGamePanelHandler>().ShowPanel();
				}
			}
			else {
				if (this.isEndGame) {
					this.isEndGame = false;
					FindObjectOfType<EndGamePanelHandler>().HidePanel();
				}
			}
		}
	}
	
	public void EndGame(){
		Debug.Log("GameEnded!");
		this.isEndGame = true;
		FindObjectOfType<EndGamePanelHandler>().ShowPanel();
	}

	[ClientRpc]
	public void RpcEndGame(){
		EndGame();
		FindObjectOfType<EndGamePanelHandler>().ShowPanel();
	}
		
}
