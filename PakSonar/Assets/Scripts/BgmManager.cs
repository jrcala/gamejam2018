﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgmManager : MonoBehaviour
{
    public static BgmManager SharedInstance;
    public List<AudioClip> BgmPlayList;
    public List<AudioClip> SfxPlayList;

    [SerializeField]
    AudioSource bgmSrc = null;
    [SerializeField]
    AudioSource sfxSrc = null;

    [SerializeField] float fadeRate = 0.5f;

    //TODO: For debug; remove later
    public bool fadeIn = false;
    public bool fadeOut = false;

    // Use this for initialization
    void OnEnable()
    {
        SharedInstance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    void Update()
    {
        //TODO: For debug; remove later
        if (fadeIn)
        {
            StartCoroutine(fadeInBgm(fadeRate));
        }
        if (fadeOut)
        {
            StartCoroutine(fadeOutBgm(fadeRate));
        }
    }

    public void changeBgm(int musIndex)
    {
        bgmSrc.clip = BgmPlayList[musIndex];
    }

    public void changeSfx(int musIndex)
    {
        sfxSrc.PlayOneShot(SfxPlayList[musIndex]);
    }

    public IEnumerator fadeInBgm(float fadeTime)
    {
        //TODO: restart music or continue?
        bgmSrc.Play();
        while (bgmSrc.volume <= 1)
        {
            bgmSrc.volume += 0.01f;
            yield return new WaitForSeconds(fadeTime);
            if (bgmSrc.volume >= 1)
            {
                break;
            }
        }
        fadeIn = false;
    }

    public IEnumerator fadeOutBgm(float fadeTime)
    {
        float currentVolume = bgmSrc.volume;
        while (bgmSrc.volume >= 0)
        {
            bgmSrc.volume -= 0.01f;
            yield return new WaitForSeconds(fadeTime);
            if (bgmSrc.volume <= 0)
            {
                break;
            }
        }
        bgmSrc.Stop();
        fadeOut = false;
    }
}
