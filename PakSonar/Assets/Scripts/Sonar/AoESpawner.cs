﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AoESpawner : MonoBehaviour
{
    [SerializeField] private GameObject pulse = null;
    [SerializeField] private float duration = 5f;
    // Use this for initialization
    void Start()
    {
		
    }
	
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(ActivatePulse());
            Instantiate(pulse, this.transform.position, this.transform.rotation);
        }
    }

    public IEnumerator ActivatePulse()
    {
        pulse.SetActive(true);
        yield return new WaitForSeconds(duration);
        pulse.SetActive(false);
    }
}
