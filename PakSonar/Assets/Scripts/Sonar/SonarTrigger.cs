﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonarTrigger : MonoBehaviour
{
    Animator anim;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void InvokeSonar()
    {
        Debug.Log("SONAR!");
        anim.Play("Echolocation");
    }

    public void HideSonar()
    {
        anim.enabled = false;
    }

    void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            InvokeSonar();
        }
    }
}
