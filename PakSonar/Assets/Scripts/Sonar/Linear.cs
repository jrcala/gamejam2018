﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Linear: MonoBehaviour
{

    //    public float rotation = 0;
    public float speed = 10.0f;

    public Vector2 direction;
    Rigidbody2D rgb2;
    // Use this for initialization
    void Start()
    {
        rgb2 = GetComponent<Rigidbody2D>();
//        targetDir = new Vector3(Mathf.Sin(Mathf.Deg2Rad * rotation), Mathf.Cos(Mathf.Deg2Rad * rotation), 0);
    }

    // Update is called once per frame
    void Update()
    {
        rgb2.velocity = direction * speed;
//        this.transform.Translate(targetDir * targetSpeed);
    }

    void OnTriggerStay2D(Collider2D coll)
    {
        Debug.Log("Hit" + coll.gameObject.name);
        if (coll.tag.Equals("Player"))
        {
            return;
        }
        else
        {
            GameObject.Destroy(this.gameObject);
        }

//        SonarTrigger trigger = coll.gameObject.GetComponent<SonarTrigger>();
//        if (trigger != null)
//        {
//            trigger.InvokeSonar();
//        }
    }
}
