﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearSpawner : MonoBehaviour
{
    public GameObject linearPrefab;
    // Use this for initialization
    void Start()
    {

    }

    void SpawnLinearSonar(Vector2 rotation)
    {
        //spawn on A
        GameObject go = Instantiate(linearPrefab, this.transform.position, transform.rotation);
        go.GetComponent<Linear>().direction = rotation;
    }

    // Update is called once per frame
    void Update()
    {
//        if (Input.GetKeyUp(KeyCode.Space))
//        {
//
//        }
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            SpawnLinearSonar(Vector2.left);
        }
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            SpawnLinearSonar(Vector2.up);
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            SpawnLinearSonar(Vector2.down);
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            SpawnLinearSonar(Vector2.right);
        }
    }
}
