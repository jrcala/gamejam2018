﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beacon: MonoBehaviour
{

    public GameObject owner;

    public float duration = 2.0f;
    private float startTime;

    public float targetScale = 1.2f;

    private float totalScale = 1.0f;
    // Use this for initialization
    void Start()
    {
        GameObject.Destroy(this.gameObject, duration);
        startTime = Time.time;
    }
	
    // Update is called once per frame
    void Update()
    {
        //totalScale *= scaleRate;
        //this.transform.localScale = Vector3.up*totalScale + Vector3.right*totalScale + Vector3.forward;
//        while (true)
//        {
        float deltaTime = (Time.time - startTime) / duration;
        this.transform.localScale = Vector3.LerpUnclamped(Vector3.one, new Vector3(targetScale, targetScale, 1), deltaTime);

//        }
 
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        //if (coll.gameObject.tag == "Enemy")
        //	coll.gameObject.SendMessage("ApplyDamage", 10);
        if (owner != coll.gameObject)
        {
            NetworkPlayerHandler trigger = coll.gameObject.GetComponent<NetworkPlayerHandler>();
            if (trigger != null)
            {
                trigger.Track(owner.GetComponent<NetworkPlayerHandler>(), 10f);
            }
        }

    }
}
