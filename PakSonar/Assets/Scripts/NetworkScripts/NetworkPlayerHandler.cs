﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[System.Serializable]
public class TrackingStruct
{

    public NetworkPlayerHandler trackedBy;
    public float timeToTrack;


    public void UpdateMe(float time)
    {
        timeToTrack -= time;
    }
}

public class NetworkPlayerHandler : NetworkBehaviour
{

    [SyncVar]
    public int score = 0;

    public GameObject sonarSpawn;
    public GameObject beaconSpawn;

    public static NetworkPlayerHandler MyOwn;
    public List<TrackingStruct> trackedBy = new List<TrackingStruct>();

    public SpriteRenderer ui;

    [SyncVar]
    public bool isShield = false;

    [SyncVar]
    public bool visibleToAll = false;

    [SyncVar]
    public int playerChara = 0;

    public GameObject blipObj;

    public List<AttackHitBox> hitboxList = new List<AttackHitBox>();

    public PlayerMovement playerMovement;
    public GameHandler gameHandler;

    public float spawnDuration = 3.0f;

    public AnimatorOverrideController[] con;

    public float sonarCooldown = 5;
    private float sonarTime = 0;

    public override void OnStartLocalPlayer()
    {
        MyOwn = this;
    }

    // Use this for initialization
    void Start()
    {
        if (!isLocalPlayer)
            ui.gameObject.SetActive(false);
        GameObject go = GameObject.Find("GameHandler");
        gameHandler = go.GetComponent<GameHandler>();

        if (isServer)
        {
            playerChara = gameHandler.currentPlayers;
        }

        gameHandler.AddPlayer();

        Animator anim = GetComponent<Animator>();
        anim.runtimeAnimatorController = con[playerChara];

    }
	
    // Update is called once per frame
    void Update()
    {
        if (!gameHandler.isGameStart)
            return;
        if (isLocalPlayer)
        {
			if (Input.GetKeyDown(KeyCode.J) && sonarTime <= 0)
            {
                BgmManager.SharedInstance.changeSfx(Random.Range(3, 6));
                GameObject Sonar = GameObject.Instantiate<GameObject>(sonarSpawn);
                Beacon b = Sonar.GetComponent<Beacon>();
                b.owner = gameObject;
                sonarTime = sonarCooldown;
                Sonar.transform.position = transform.position;
                CmdSpawnBlip();
            }

            if (sonarTime > 0)
                sonarTime -= Time.deltaTime;
			if (Input.GetKeyDown(KeyCode.K))
            {
                BgmManager.SharedInstance.changeSfx(0);
                foreach (AttackHitBox hitbox in hitboxList)
                {
                    if (hitbox.facing == playerMovement.facing)
                    {
                        foreach (GameObject target in hitbox.targets)
                        {
                            BgmManager.SharedInstance.changeSfx(2);
                            CmdKill(target, gameObject);
                        }
                        break;
                    }
                }
                //foreach(GameObject target in hitboxList){
                //if(tar)
                //}
            }

        }
        else
        {
            if (trackedBy.Exists(x => x.trackedBy == MyOwn) || visibleToAll)
            {
                if (this.isShield)
                {
                    trackedBy.Clear();
                    visibleToAll = false;
                }
                ui.gameObject.SetActive(true);
            }
            else
            {
                ui.gameObject.SetActive(false);
            }
        }

        if (trackedBy.Count > 0)
        {
            for (int i = 0; i < trackedBy.Count; i++)
            {
                trackedBy[i].UpdateMe(Time.deltaTime);
            }
            trackedBy.RemoveAll(x => x.timeToTrack <= 0);
        }
    }

    [Command]
    public void CmdBeaconSpawn(GameObject sender)
    {
        GameObject b = Instantiate<GameObject>(beaconSpawn);
        b.transform.position = sender.gameObject.transform.position;
        b.GetComponent<NetworkBeacon>().owner = sender;

        NetworkServer.Spawn(b);
    }

    [Command]
    public void CmdKill(GameObject enemy, GameObject sender)
    {
        if (isServer)
        {
            sender.GetComponent<NetworkPlayerHandler>().score += 1;
        }
        RpcKill(enemy);
        //GameObject.Destroy(enemy);
        enemy.GetComponent<NetworkPlayerHandler>().Die();
    }

    [ClientRpc]
    public void RpcKill(GameObject enemy)
    {
        enemy.GetComponent<NetworkPlayerHandler>().Die();
    }

    public void Die()
    {
        this.transform.position = new Vector3(999999.0f, 999999.0f, 0);
        this.playerMovement.enabled = false;
        if (this.isLocalPlayer)
        {
            this.Invoke("Live", spawnDuration);
        }
    }

    public void Live()
    {
        gameHandler.Respawn(this);
        this.playerMovement.enabled = true;
    }

    public void ActivateShield(float duration)
    {
        this.isShield = true;
        this.Invoke("DeactivateShield", duration);
    }

    public void DeactivateShield()
    {
        isShield = false;
    }

    public void Track(NetworkPlayerHandler trackSource, float trackTime)
    {
        if (isShield)
        {// don't track if player is shielded
            return;
        }
        TrackingStruct newTrack = new TrackingStruct();
        newTrack.trackedBy = trackSource;
        newTrack.timeToTrack = trackTime;

        if (!trackedBy.Exists(x => x.trackedBy == trackSource))
        {
            trackedBy.Add(newTrack);
        }
        else
        {
            trackedBy.Find(x => x.trackedBy == trackSource).timeToTrack = trackTime;
        }

        Debug.Log("Track " + gameObject.name + " is being tracked by " + trackSource.gameObject.name);
    }

    [Command]
    public void CmdSpawnBlip()
    {
        GameObject obj = GameObject.Instantiate<GameObject>(blipObj);
        obj.transform.position = transform.position;

        NetworkServer.Spawn(obj);
    }
}
