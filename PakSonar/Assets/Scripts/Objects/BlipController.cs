﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BlipController : NetworkBehaviour{

	public float duration = 2.0f;

	// Use this for initialization
	void Start () {
		GameObject.Destroy(gameObject, duration);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
