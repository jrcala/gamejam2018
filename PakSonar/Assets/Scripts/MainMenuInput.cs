﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuInput : MonoBehaviour
{
    public string toMain;
    public bool isStartScreen = true;

    // Update is called once per frame
    void LateUpdate()
    {
        if (Input.anyKeyDown && isStartScreen)
        {
            //only works if its in the startscreen
            SceneManager.LoadScene(toMain);
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadGame(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}
