﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PowerupsSpawner : NetworkBehaviour {

	public class PowerupSpawnData{
		public Transform trans;
		public bool active = false;
		public int count = 0;
	}
	//private float spawnDuration = 5.0f;
	private int maxItems = 2;

	public GameObject powerUpPickupPrefab;

	public List<Transform> spawnPoints = new List<Transform>();

	public List<PowerupSpawnData> spawnDataList = new List<PowerupSpawnData>();
	// Use this for initialization
	void Start () {
		if(this.isServer){
			foreach(Transform t in spawnPoints){
				spawnDataList.Add(new PowerupSpawnData{
					trans  = t
				});
			}
			spawnDataList.Sort(delegate(PowerupSpawnData x,  PowerupSpawnData y)
			{
				if(Random.value > 0.5f){
					return 1;
				}
				else{
					return -1;
				}
			});
			TriggerSpawn();
			TriggerSpawn();
		}
	}

	public void TriggerSpawn(){
		spawnDataList.Sort(delegate(PowerupSpawnData x,  PowerupSpawnData y)
		{
				if(x.active) return 1;
				if(y.active) return -1;
				if(x.count < y.count) return -1;
				if(x.count > y.count) return 1;
				if(x.count == y.count) return 0;
				return 0;
		});
		PowerupSpawnData spawnData = spawnDataList[0];
		spawnData.count += 1;
		GameObject go = GameObject.Instantiate(powerUpPickupPrefab);
		go.transform.position = spawnData.trans.position;
		spawnData.active = true;
		go.GetComponent<PowerupPickup>().spawner = this;
		go.GetComponent<PowerupPickup>().spawnData = spawnData;
		NetworkServer.Spawn(go);
	}
}
