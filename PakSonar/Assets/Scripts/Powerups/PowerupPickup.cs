﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PowerupPickup : NetworkBehaviour
{
    public enum Powerups
    {
        SHIELD = 0,
        SPEED = 1,
        BEACON = 2,
        NONE = -1
    }

    public Sprite[] sprites;

    public SpriteRenderer renderSprite;

    public Powerups powerup = Powerups.SHIELD;
    public PowerupsSpawner spawner;
    public PowerupsSpawner.PowerupSpawnData spawnData;

    void Start()
    {
        powerup = (Powerups)Random.Range(0, 3);
        renderSprite.sprite = sprites[(int)powerup]; 
    }


    void OnTriggerEnter2D(Collider2D coll)
    {
        if (this.isServer)
        {
            NetworkPlayerHandler trigger = coll.gameObject.GetComponent<NetworkPlayerHandler>();
            if (trigger != null)
            {
                trigger.playerMovement.PickupPowerup(powerup);
                spawner.TriggerSpawn();
                this.RpcDestroy();
                spawnData.active = false;
                GameObject.Destroy(this.gameObject);
            }
        }
        if (coll.gameObject.GetComponent<NetworkPlayerHandler>() == NetworkPlayerHandler.MyOwn)
        {
			NetworkPlayerHandler trigger = coll.gameObject.GetComponent<NetworkPlayerHandler>();
			if (trigger != null)
			{
				trigger.playerMovement.PickupPowerup(powerup);
			}
            BgmManager.SharedInstance.changeSfx(Random.Range(3, 6));

        }

                    			
    }

    [RPC]
    public void RpcDestroy()
    {
        GameObject.Destroy(this.gameObject);
    }
}
