﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Shield : NetworkBehaviour {
	public float duration = 5.0f;
	void OnTriggerEnter2D(Collider2D coll) {
		if(this.isServer){
			NetworkPlayerHandler trigger = coll.gameObject.GetComponent<NetworkPlayerHandler>();
			if (trigger != null) {
				trigger.ActivateShield(duration);
				this.RpcDestroy();
				GameObject.Destroy(this.gameObject);
			}
		}			
	}

	[RPC]
	public void RpcDestroy(){
		GameObject.Destroy(this.gameObject);
	}
}
