﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField]private string action = null;
    // Use this for initialization
    void OnCollisionEnter2D(Collision2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            hit.gameObject.BroadcastMessage("ActivatePower", action);
            Destroy(this.gameObject);
        }
    }
}
