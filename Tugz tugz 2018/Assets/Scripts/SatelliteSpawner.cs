﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SatelliteSpawner : MonoBehaviour
{
    [SerializeField] private int spawnLimit = 5;
    [SerializeField] private int objectsSpawned = 0;
    [SerializeField] private float radius = 1;
    [SerializeField] private float minHeight = 5;
    [SerializeField] private float spawnInterval = 2;
    [SerializeField] private GameObject Satellite = null;
    [SerializeField] private Color radColor = Color.yellow;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(spawnSatellite());
    }

    public IEnumerator spawnSatellite()
    {
        while (objectsSpawned < spawnLimit)
        {
            yield return new WaitForSeconds(spawnInterval);
            Vector3 spawnLocation = new Vector3(Random.Range(-radius, radius), Random.Range(transform.position.y + minHeight, radius), Random.Range(-radius, radius));
            Instantiate(Satellite, spawnLocation, this.transform.localRotation);
            objectsSpawned++;
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = radColor;
        Gizmos.DrawSphere(transform.position, radius + 5);
    }
}
