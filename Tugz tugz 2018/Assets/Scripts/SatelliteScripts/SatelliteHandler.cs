﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SatelliteStatuses {
	IDLE,
	TRANSMITTING,
	BLOCKING
}

public class SatelliteHandler : MonoBehaviour {

	public Color IdleColor = Color.white;
	public Color TransmittingColor = Color.red;
	public Color BlockingColor = Color.yellow;

	[SerializeField]
	private bool _transmitting = false;
	public bool Transmitting {
		get {
			return _transmitting;
		}
	}

	[SerializeField]
	private SatelliteStatuses _satelliteStatus = SatelliteStatuses.IDLE;
	public SatelliteStatuses SatelliteStatus {
		get {
			return _satelliteStatus;
		}
	}

	[SerializeField]
	private ControllerInput _neededInput;
	public ControllerInput NeededInput {
		get {
			return _neededInput;
		}
	}

	[SerializeField]
	private float _transmissionTime;
	/// <summary>
	/// How long will this thing transmit
	/// </summary>
	public float TransmissionTime {
		get {
			return _transmissionTime;
		}
	}

	[SerializeField]
	private float _blockTime;
	/// <summary>
	/// How long until you block this thing
	/// </summary>
	public float BlockTime {
		get {
			return _blockTime;
		}
	}

	private Renderer[] renderers;

	private float _currBlockTime;
	private float _currTransmitTime;

	private InputHandler inputHandler;
	
	public delegate void VoidActionHandler(SatelliteHandler sender);
	public event VoidActionHandler OnSuccesfulBlock;
	public event VoidActionHandler OnFailBlock;

	void OnEnable() {
		inputHandler = InputHandler.Instance;
		renderers = GetComponentsInChildren<Renderer>();

		OnSuccesfulBlock += SatelliteHandler_OnSuccesfulBlock;
		OnFailBlock += SatelliteHandler_OnFailBlock;
	}

	void OnDisable() {
		OnSuccesfulBlock -= SatelliteHandler_OnSuccesfulBlock;
		OnFailBlock -= SatelliteHandler_OnFailBlock;
	}

	private void ResetVariables() {
		_transmissionTime = 0;
		_currBlockTime = 0;
		_currTransmitTime = 0;

		_satelliteStatus = SatelliteStatuses.IDLE;
	}

	private void SatelliteHandler_OnFailBlock(SatelliteHandler sender) {
		ResetVariables();
	}

	private void SatelliteHandler_OnSuccesfulBlock(SatelliteHandler sender) {
		ResetVariables();
	}

	public void AssignRandomButton() {
		_neededInput = inputHandler.GetRandomButton();
	}

	public void Transmit(float time, float blockTime) {
		_transmissionTime = time;
		_blockTime = blockTime;
		AssignRandomButton();
	}

	public void ChangeColor(Color c) {
		foreach (Renderer r in renderers) {
			r.material.color = c;
		}
	}

	public void UpdateUI() {
		switch (SatelliteStatus) {
			case SatelliteStatuses.IDLE:
				ChangeColor(IdleColor); break;
			case SatelliteStatuses.TRANSMITTING:
				ChangeColor(TransmittingColor); break;
			case SatelliteStatuses.BLOCKING:
				ChangeColor(BlockingColor); break;
		}
	}

	public void UpdateStatus() {
		if(TransmissionTime > 0) {
			bool isBlocked = inputHandler.CheckButtonPressed(NeededInput);

			if (isBlocked) {
				_satelliteStatus = SatelliteStatuses.BLOCKING;
				_currBlockTime += Time.deltaTime;
				if(_currBlockTime >= BlockTime) {
					OnSuccesfulBlock(this);
				}
			}
			else {
				_satelliteStatus = SatelliteStatuses.TRANSMITTING;
				_currTransmitTime += Time.deltaTime;

				///If player released prematurely FAIL desu
				if(_currTransmitTime >= TransmissionTime || _currBlockTime > 0) {
					OnFailBlock(this);
				}
			}
        }
		else {
			_satelliteStatus = SatelliteStatuses.IDLE;
		}

		UpdateUI();
	}
	
	// Update is called once per frame
	void Update () {
		UpdateStatus();
	}
}
