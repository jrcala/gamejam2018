﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SatelliteHandler))]
public class SatelliteHandlerEditor : Editor {
	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		SatelliteHandler t = target as SatelliteHandler;
		if (GUILayout.Button("Assign Random Button")) {
			t.AssignRandomButton();
		}

		if (GUILayout.Button("Transmit")) {
			t.Transmit(3, 5);
		}
	}
}
