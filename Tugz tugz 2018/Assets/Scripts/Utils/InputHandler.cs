﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ButtonAxes {
	POSITIVE,
	NEGATIVE
}

[System.Serializable]
public class ControllerInput {
	public string name = "Button";
	public KeyCode inputButton;
	public string buttonName;
	/// <summary>
	/// Use this when using Axis- Else leave blank
	/// </summary>
	public string buttonID = "";
	/// <summary>
	/// Used when using Axis- either pos or neg
	/// </summary>
	public ButtonAxes ButtonAxis = ButtonAxes.POSITIVE;
}

public class InputHandler : MonoBehaviour {

	public static InputHandler Instance;
	public List<ControllerInput> buttonMap;

	void OnEnable() {
		Instance = this;
	}

	// Use this for initialization
	void Start () {
			
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public ControllerInput GetRandomButton() {
		int index = Random.Range(0, buttonMap.Count);
		if(index < buttonMap.Count) {
			return buttonMap[index];
		}
		else {
			return buttonMap[0];
		}
	}

	public bool CheckButtonPressed(ControllerInput toCheck) {
		bool ret = false;

		if (string.IsNullOrEmpty(toCheck.buttonID)) {
			ret = Input.GetKey(toCheck.inputButton);
		}
		else {
			float val = Input.GetAxis(toCheck.buttonID);
			ret = toCheck.ButtonAxis == ButtonAxes.POSITIVE ? val > 0 : val < 0;
		}

		return ret;
	}
}
